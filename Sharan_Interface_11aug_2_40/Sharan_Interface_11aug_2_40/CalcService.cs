﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Interface_11aug_2_40
{
    class CalcService : ICalc,ISample
    {
        public int add(int num1, int num2)
        {
            return num1 + num2;
        }

        public double div(int num1, int num2)
        {
            return num1 / num2;
        }

        public int mul(int num1, int num2)
        {
            return num1 * num2;
        }

        public void prin()
        {
            Console.WriteLine("hi");
        }

        public int sub(int num1, int num2)
        {
            return num1 - num2;
        }
    }
}
