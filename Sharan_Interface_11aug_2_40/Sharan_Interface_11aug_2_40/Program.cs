﻿using System;

namespace Sharan_Interface_11aug_2_40
{
    class Program
    {
        static void Main(string[] args)
        {
            CalcService cal = new CalcService();
            int num1 = 1, num2 = 2;
            Console.WriteLine($"Add: {cal.add(2, 2)}\nSub: {cal.sub(2, 1)}\nDiv: {cal.div(4,2)}\nMul: {cal.mul(2,2)}");
            cal.prin();
            try
            {
                if(num1 > num2)
                {
                    Console.WriteLine($"Div: {cal.div(num1, num2)}");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
/* real time examples 10 - interface,abstraction, */