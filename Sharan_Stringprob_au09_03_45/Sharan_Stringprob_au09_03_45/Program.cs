﻿using System;

namespace Sharan_Stringprob_au09_03_45
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your name : ");
            var name = Console.ReadLine();
            var ans = name.Substring(name.Length-5);
            Console.WriteLine($"Last five letters of your name is {ans}");
        }
    }
}
