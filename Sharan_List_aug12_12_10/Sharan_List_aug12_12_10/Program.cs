﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sharan_List_aug12_12_10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>()
            {
                new Employee(1,"mauwalla",10000),
                new Employee(2,"sai",10000),
                new Employee(3,"bhargav",10000)
            };
            employees.ForEach(x => Console.WriteLine(x.Id+" "+x.Name+" "+x.Salary));

            ArrayList empList = new ArrayList();
            empList.Add(new Employee(1,"mauwalla",1000));
            empList.Add(new Employee(2, "sai", 10000));
            empList.Add(new Employee(3, "bhargav", 10000));
        }
    }
}
