﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_List_aug12_12_10
{
    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }

        public Employee(int _id,string _name,int _salary)
        {
            Id = _id;
            Name = _name;
            Salary = _salary;
        }

        public string getPerson()
        {
            return new Employee(Id, Name, Salary).ToString();
        }
    }
}
