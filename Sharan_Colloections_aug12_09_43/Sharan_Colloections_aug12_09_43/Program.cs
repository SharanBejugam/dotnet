﻿using System;
using System.Collections;

namespace Sharan_Colloections_aug12_09_43
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(10);
            list.Add("Test");
            list.Add(2.90);

            IEnumerator iEnum = list.GetEnumerator();

            string msg = "";
            while(iEnum.MoveNext())
            {
                msg += iEnum.Current.ToString() + "\n";
            }
            Console.WriteLine(msg);


            string[] list1 = new string[list.Count];
            list1 = (string[])list.Clone();
            

            ArrayList arr = new ArrayList();
            arr.Add(list);

            IEnumerator inum = arr.GetEnumerator();
            while(inum.MoveNext())
            Console.WriteLine(inum.Current.ToString());

        }
    }
}

/* Enummerator and Enummerable and Icollections */