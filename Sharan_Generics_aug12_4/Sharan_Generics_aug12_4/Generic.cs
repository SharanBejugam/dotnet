﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Generics_aug12_4
{
    class Generic 
    {
        public void Drop<T1,T2>(T1 param1,T2 param2)
        {
            Console.WriteLine($"Param1: {param1}, Param2: {param2}");
        }
    }
}
