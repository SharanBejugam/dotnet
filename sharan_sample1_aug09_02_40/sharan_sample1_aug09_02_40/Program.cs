﻿using System;

namespace sharan_sample1_aug09_02_40
{
    class Program
    {
        static void Main(string[] args)
        {
            var op = new Operation();
            Console.WriteLine("Hello World!");
            int FirstVal;
            int SecondVal;
            Console.WriteLine("Enter the first number:");
            FirstVal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter second number");
            SecondVal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Addition of {FirstVal} and {SecondVal} numbers is {op.Addition(FirstVal, SecondVal)}");
        }
    }

    class Operation
    {
        public int Addition(int first, int secont)
        {
            return first + secont;
        }
    }
}
