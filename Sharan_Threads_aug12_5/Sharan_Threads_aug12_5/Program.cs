﻿using System;
using System.Threading;

namespace Sharan_Threads_aug12_5
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = new Threads();
            Thread t1 = new Thread(t.work1);
            Thread t2 = new Thread(t.work2);

            t1.Start();
            t2.Start();
        }
    }
}
