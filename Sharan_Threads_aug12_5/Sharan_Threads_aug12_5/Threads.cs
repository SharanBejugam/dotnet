﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Threads_aug12_5
{
    class Threads
    {
        public void work1()
        {
            long sum = 0;
            for(int i=0;i<100;i++)
            {
                sum += i;
            }
            Console.WriteLine(sum);
        }

        public void work2()
        {
            long sum = 0;
            for (int i = 0; i < 200; i++)
            {
                sum += i;
            }
            Console.WriteLine(sum);
        }
    }
}
