﻿using System;

namespace sharan_abstract_11_11_30
{
    class Program
    {
        static void Main(string[] args)
        {
            var rec = new Rectangle();
            Console.WriteLine("Area: " + rec.Area(5));
            var elec = new Laptop();
            Console.WriteLine(elec.showDetails("Dell", 10000));
        }
    }

    abstract class Shape
    {
        public abstract int Area(int side);
        public abstract string ShapeName();
    }

    class Rectangle : Shape
    {
        public override int Area(int side)
        {
            return side * side;
        }

        public override string ShapeName()
        {
            throw new NotImplementedException();
        }
    }
}
