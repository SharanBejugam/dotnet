﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sharan_abstract_11_11_30
{
    abstract class Electronics
    {
        public abstract string showDetails(string name, int price);
    }

    class Laptop : Electronics
    {
        public override string showDetails(string name, int price)
        {
            return $"Name: {name}\nPrice: {price}";
        }
    }
}
