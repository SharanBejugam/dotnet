﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Inheritance_11_09_30
{
    class Library
    {
        public string Genere { get; set; }
    }

    class Books : Library
    {
        public string Name { get; set; }
        public void getDetails()
        {
            Console.WriteLine($"Genere: {Genere}\n Name: {Name}");
        }
    }
}
