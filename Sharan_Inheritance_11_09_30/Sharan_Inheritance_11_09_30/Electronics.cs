﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Inheritance_11_09_30
{
    class Electronics
    {
        public string Type { get; set; }
    }

    class Computer : Electronics
    {
        public string Name { get; set; }
        public void getDetails()
        {
            Console.WriteLine($"Type: {Type}\n Name: {Name}");
        }
    }
}
