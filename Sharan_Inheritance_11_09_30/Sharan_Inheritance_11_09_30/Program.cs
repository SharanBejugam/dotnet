﻿using System;

namespace Sharan_Inheritance_11_09_30
{
    class Program
    {
        static void Main(string[] args)
        {
            var stud = new Student()
            {
                Name = "Sharan",
                Id = 12,
                Course = ".NET",
                Class = 10
            };
            stud.Getdetails();
        }
    }

    class Person
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }

    class Student : Person
    {
        public string Course { get; set; }
        public int Class { get; set; }
        public void Getdetails()
        {
            Console.WriteLine($"Name: {Name} Id: {Id}\n Course: {Course}\n Class: {Class}");
        }
    }
}
