﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_Inheritance_11_09_30
{
    class Vehicles
    {
        public int VehicleType { get; set; }
    }

    class Car : Vehicles
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public void GetDetails()
        {
            Console.WriteLine($"Vehicle Type: {VehicleType}\n Id: {Id}\n Name: {Name}");
        }
    }
}
