﻿using System;
using System.IO;
namespace Sharan_Files_aug12_04_23
{
    class Program
    {
        class FileWrite
        {
            public void write()
            {
                FileStream fs = new FileStream(@"E:\Sample.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                string str = "Hello HI";
                sw.WriteLine(str);
                sw.Flush();
                sw.Close();
                fs.Close();
            }

            public void read()
            {
                FileStream fs = new FileStream(@"E:\Sample.txt", FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
                fs.Close();
            }
        }

        static void Main(string[] args)
        {
            FileWrite wr = new FileWrite();
            wr.write();
            wr.read();
        }
    }
}
