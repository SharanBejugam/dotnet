﻿using System;

namespace Sharan_Subraction_aug09_03_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Enter First number : ");
            var firstval = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Enter Second number : ");
            var Secondval = Convert.ToInt32(Console.ReadLine());
            int ans;
            if(firstval > Secondval)
            {
                ans = firstval - Secondval;
            }
            else
            {
                ans = Secondval - firstval;
            }
            Console.WriteLine($"Subraction : {ans}");
        }
    }
}
