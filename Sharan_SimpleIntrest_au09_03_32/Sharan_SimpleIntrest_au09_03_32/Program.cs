﻿using System;

namespace Sharan_SimpleIntrest_au09_03_32
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Principle amount : ");
            var prinamu = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter tenure in years : ");
            var tenure = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter interst percent : ");
            var intrest = Convert.ToDouble(Console.ReadLine());
            var ans = (prinamu * tenure * intrest) / 100;
            Console.WriteLine($"Simple Intrest : {ans}");
        }
    }
}
