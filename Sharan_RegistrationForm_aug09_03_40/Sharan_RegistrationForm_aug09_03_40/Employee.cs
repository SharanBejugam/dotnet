﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sharan_RegistrationForm_aug09_03_40
{
    class Employee
    {
        private int id;
        private string name;
        private string email;
        private long mobilenumber;
        private DateTime DateOfBirth;
        private string location;
        public Employee(int _id,string _name,string _email,long _mobilenumber,DateTime _DateOfBirth,string _location)
        {
            id = _id;
            name = _name;
            email = _email;
            mobilenumber = _mobilenumber;
            DateOfBirth = _DateOfBirth;
            location = _location;
        }
    }
}
