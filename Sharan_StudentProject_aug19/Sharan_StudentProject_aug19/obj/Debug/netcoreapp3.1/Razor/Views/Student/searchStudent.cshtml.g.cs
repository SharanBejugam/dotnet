#pragma checksum "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\Student\searchStudent.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "af82283fe09e8f8c78df17915e27ae07f4bc059c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Student_searchStudent), @"mvc.1.0.view", @"/Views/Student/searchStudent.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\_ViewImports.cshtml"
using Sharan_StudentProject_aug19;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\_ViewImports.cshtml"
using Sharan_StudentProject_aug19.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"af82283fe09e8f8c78df17915e27ae07f4bc059c", @"/Views/Student/searchStudent.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0721e3d2addcd7e5bde156246f9cfce3f77767de", @"/Views/_ViewImports.cshtml")]
    public class Views_Student_searchStudent : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Student>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<h2>Search Student</h2>\r\n\r\n");
            WriteLiteral("<div>\r\n");
#nullable restore
#line 5 "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\Student\searchStudent.cshtml"
     using (Html.BeginForm())
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"row form-group\">\r\n            <div class=\"col-md-6\">\r\n                ");
#nullable restore
#line 9 "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\Student\searchStudent.cshtml"
           Write(Html.EditorFor(x => x.StudId, new { htmlAttributes = new { @class = "form-control", placeholder = @Html.DisplayNameFor(m => m.StudId) } }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <input type=\"submit\" value=\"Get\" class=\"btn btn-primary\" />\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 15 "C:\Users\eystack12\source\repos\Sharan_StudentProject_aug19\Sharan_StudentProject_aug19\Views\Student\searchStudent.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Student> Html { get; private set; }
    }
}
#pragma warning restore 1591
