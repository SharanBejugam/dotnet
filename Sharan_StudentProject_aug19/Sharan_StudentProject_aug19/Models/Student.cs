﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharan_StudentProject_aug19.Models
{
    public class Student
    {
        public Guid StudId { get; set; }
        public string StudFirstName { get; set; }
        public string StudLastName { get; set; }
        public long StudPhone { get; set; }
        public DateTime StudDob { get; set; }
        public string StudAddress { get; set; }
        public string StudEmail { get; set; }
        public DateTime StudDoj { get; set; }
        public string StudCourse { get; set; }
        public string StudStream { get; set; }

    }
}
