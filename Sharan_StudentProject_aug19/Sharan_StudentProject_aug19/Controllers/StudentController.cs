﻿using Microsoft.AspNetCore.Mvc;
using Sharan_StudentProject_aug19.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sharan_StudentProject_aug19.Controllers
{
    public class StudentController : Controller
    {
        public static List<Student> _students = new List<Student>
        {
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "ANJU",
                StudLastName = "PAUL",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud1@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "BTech",
                StudStream = "CSE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "KARTHIK",
                StudLastName = "KULKARNI",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud2@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "ECE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "GUMMIREDDY",
                StudLastName = "SUNEETHI",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud3@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "CSE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "Sandeep",
                StudLastName = "Varma",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud4@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "BBA",
                StudStream = "Finance"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "NEHA",
                StudLastName = "NEHA",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud5@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MBA",
                StudStream = "HR"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "GEETHU",
                StudLastName = "S",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud6@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MBA",
                StudStream = "Finance"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "SAHANA",
                StudLastName = "SAHANA",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud7@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "BTech",
                StudStream = "EEE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "CHAITRA",
                StudLastName = "RAO",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud8@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "EEE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "LOKESH",
                StudLastName = "M",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud9@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "ECE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "NISHANT",
                StudLastName = "PATIL",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud10@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "CSE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "VARSHA",
                StudLastName = "V",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud11@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MBA",
                StudStream = "HR"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "SACHIN",
                StudLastName = "K",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud12@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "BTech",
                StudStream = "CSE"
            },
            new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = "KUSHAL",
                StudLastName = "M",
                StudPhone = 1234567890,
                StudDob = new DateTime(2020,08,21).Date,
                StudAddress = "Ground Floor, ‘A’ wing, Divyasree Chambers, # 11, O’Shaughnessy Road, Langford Gardens,Bengaluru 560025",
                StudEmail = "stud13@gmail.com",
                StudDoj = new DateTime(2022,08,21).Date,
                StudCourse = "MTech",
                StudStream = "ECE"
            },
        };

        public IActionResult displayStudents()
        {
            ViewBag.studList = _students;
            return View();
        }

        public IActionResult addStudent()
        {
            return View();
        }

        [HttpPost]
        public IActionResult addStudent(Student input)
        {
            _students.Add(new Student()
            {
                StudId = Guid.NewGuid(),
                StudFirstName = input.StudFirstName,
                StudLastName = input.StudLastName,
                StudDob = input.StudDob,
                StudAddress = input.StudAddress,
                StudEmail = input.StudEmail,
                StudDoj = input.StudDoj,
                StudCourse = input.StudCourse,
                StudStream = input.StudStream,
            });
            return RedirectToAction("displayStudents");
        }

        public IActionResult deleteStudent()
        {
            return View();
        }

        [HttpPost]
        public IActionResult deleteStudent(Guid id)
        {
            _students.Remove(_students.SingleOrDefault(x => x.StudId == id));
            return RedirectToAction("displayStudents");
        }

        public IActionResult searchStudent()
        {
            return View();
        }

        [HttpPost]
        public IActionResult searchStudent(Guid id)
        {
            ViewBag.student = _students.SingleOrDefault(x => x.StudId == id);
            return View();
        }

        [HttpGet]
        public ActionResult modifyStudent(Guid id)
        {
            return View(_students.SingleOrDefault(x => x.StudId == id));
        }

        [HttpPost]
        public ActionResult modifyStudent(Student std)
        {
            var student = _students.Where(x => x.StudId == std.StudId).SingleOrDefault();
            if (std.StudFirstName != null && student.StudFirstName != null) { student.StudFirstName = std.StudFirstName; }
            if (std.StudLastName != null && student.StudLastName != null) { student.StudLastName = std.StudLastName; }
            if (std.StudPhone != null && student.StudPhone != null) { student.StudPhone = std.StudPhone; }
            if (std.StudDob != null && student.StudDob != null) { student.StudDob = std.StudDob; }
            if (std.StudAddress != null && student.StudAddress != null) { student.StudAddress = std.StudAddress; }
            if (std.StudEmail != null && student.StudEmail != null) { student.StudEmail = std.StudEmail; }
            if (std.StudCourse != null && student.StudCourse != null) { student.StudCourse = std.StudCourse; }
            if (std.StudStream != null && student.StudStream != null) { student.StudStream = std.StudStream; }
            return RedirectToAction("displayStudents");
        }
    }
}