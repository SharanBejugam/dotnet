﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sharan_StudentProject_aug19.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Sharan_StudentProject_aug19.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult addStudent()
        {
            return RedirectToAction("addStudent", "StudentController");
        }

        public IActionResult modifyStudent()
        {
            return RedirectToAction("modifyStudent", "StudentController");
        }

        public IActionResult searchStudent()
        {
            return RedirectToAction("searchStudent", "StudentController");
        }

        public IActionResult displayStudents()
        {
            return RedirectToAction("displayStudents", "StudentController");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
